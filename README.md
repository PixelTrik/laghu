# Laghu

Laghu (meaning 'small' in Sanskrit) is a small, 32-bit multitasking operating
system for RISC V instruction set architecture. After learning about how
operating systems are made by making
[ArukaOS](https://gitlab.com/PixelTrik/arukaos) a year ago, I am now attempting
to write a Unix-like operating system for the open instruction set
architecture.

## Why RISC V?
CPU instruction sets were historically closed under legal barriers of companies
as intellectual property with little information on how the system is supposed
to work. Developed in 2010 by UC Berkeley as a teaching tool with
modern paradigms, RISC V has expanded from academia to the industry providing
an open, simple, reduced instruction set for computers that can be commonly
shared by hardware developers of today. What's interesting about RISC V is it
being a straight forward architecture for writing operating systems, providing
less reliance on low-level hacking as needed to be done for x86 processors.

## The Current State of Project
Currently, the project is on halt due to various events in personal and 
professional life. As a result, I am barely able to give that this project 
needs. Right now, I'm currently implementing block driver. You can follow the 
[dev tree](https://gitlab.com/PixelTrik/laghu/-/tree/dev) to refer the updates 
in this project.